package com.example.rothnaksomang.homework8.entity.maincategory;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class SubCateItem{

	@SerializedName("des")
	private String des;

	@SerializedName("cate_name")
	private String cateName;

	@SerializedName("total_url")
	private int totalUrl;

	@SerializedName("id")
	private int id;

	@SerializedName("icon_name")
	private Object iconName;

	@SerializedName("status")
	private boolean status;

	public void setDes(String des){
		this.des = des;
	}

	public String getDes(){
		return des;
	}

	public void setCateName(String cateName){
		this.cateName = cateName;
	}

	public String getCateName(){
		return cateName;
	}

	public void setTotalUrl(int totalUrl){
		this.totalUrl = totalUrl;
	}

	public int getTotalUrl(){
		return totalUrl;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIconName(Object iconName){
		this.iconName = iconName;
	}

	public Object getIconName(){
		return iconName;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SubCateItem{" + 
			"des = '" + des + '\'' + 
			",cate_name = '" + cateName + '\'' + 
			",total_url = '" + totalUrl + '\'' + 
			",id = '" + id + '\'' + 
			",icon_name = '" + iconName + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}