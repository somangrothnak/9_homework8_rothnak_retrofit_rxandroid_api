package com.example.rothnaksomang.homework8.extendableadapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rothnaksomang.homework8.R;
import com.example.rothnaksomang.homework8.entity.maincategory.DataItem;
import com.example.rothnaksomang.homework8.entity.maincategory.SubCateItem;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;

public class ExpandableAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<DataItem> listDataHeader;
    private HashMap<String, List<SubCateItem>> listHashMap;

    public ExpandableAdapter(Context context, List<DataItem> listDataHeader, HashMap<String, List<SubCateItem>> listHashMap) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listHashMap = listHashMap;
    }

    @Override
    public int getGroupCount() {
        return listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return listHashMap.get(listDataHeader.get(groupPosition).getCateName()).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listHashMap.get(listDataHeader.get(groupPosition).getCateName()).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        DataItem headerTitle= (DataItem) getGroup(groupPosition);
        if(convertView==null){
            LayoutInflater inflater= (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.list_group,null);
        }
        TextView tvTitle=convertView.findViewById(R.id.tv_header_title);
        ImageView ivMainCategory=convertView.findViewById(R.id.iv_main_category);

        LinearLayout linearLayout=convertView.findViewById(R.id.ll_item);

        loadImageFromURL(headerTitle.getIconName(),ivMainCategory);

        tvTitle.setTypeface(null,Typeface.BOLD);
        tvTitle.setText(headerTitle.getCateName());

        return convertView;
    }

    public void loadImageFromURL(String fileUrl, ImageView iv){
        Picasso.with(context).load(fileUrl).into(iv);
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final  SubCateItem childText= (SubCateItem) getChild(groupPosition,childPosition);
        if(convertView==null){
            LayoutInflater inflater= (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.list_item,null);
        }
        TextView tvChildTitle=convertView.findViewById(R.id.tv_child_title);
        LinearLayout linearLayout=convertView.findViewById(R.id.ll_subCategory);

        tvChildTitle.setText(childText.getCateName());

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,childText.getCateName(),Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }



}
