package com.example.rothnaksomang.homework8.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.rothnaksomang.homework8.R;
import com.example.rothnaksomang.homework8.callback.MainCategoryCallback;

public class MainCategoryUpdateAdapter extends DialogFragment {
    MainCategoryCallback mainCategoryCallback;
    LayoutInflater inflater;

    public MainCategoryUpdateAdapter(){}


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainCategoryCallback= (MainCategoryCallback) context;
        inflater.from(context);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        View view=inflater.inflate(R.layout.layout_main_item_update,null);
        builder.setTitle("Update Main Category");
        builder.setView(view);

        final DialogViewHolder dialogViewHolder=new DialogViewHolder(view);

        return builder.create();
    }

    class DialogViewHolder extends RecyclerView.ViewHolder{
        ImageButton imgMainCategory;
        EditText etTitle;
        Button btnUpdate;


        public DialogViewHolder(@NonNull View itemView) {
            super(itemView);
            imgMainCategory=itemView.findViewById(R.id.img_main_category);
            etTitle=itemView.findViewById(R.id.et_main_title);
            btnUpdate=itemView.findViewById(R.id.btn_update);

            btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(),"Dialog",Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}
