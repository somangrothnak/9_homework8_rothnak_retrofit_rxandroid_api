package com.example.rothnaksomang.homework8.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.example.rothnaksomang.homework8.R;
import com.example.rothnaksomang.homework8.callback.MainCategoryCallback;
import com.example.rothnaksomang.homework8.entity.maincategory.DataItem;
import com.example.rothnaksomang.homework8.entity.maincategory.SubCateItem;
import com.example.rothnaksomang.homework8.extendableadapter.ExpandableAdapter;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentMain extends Fragment {
    ExpandableAdapter expandableAdapter;
    List<DataItem> listDataHeader;
    HashMap<String,List<SubCateItem>> listDataChild;
    LayoutInflater inflater;
    ExpandableListView expandableListView;
    MainCategoryCallback mainCategoryCallback;

    public FragmentMain(){
        mainCategoryCallback= (MainCategoryCallback) getActivity();
    }

    public void setValue(List<DataItem> listDataHeader, HashMap<String, List<SubCateItem>> listDataChild) {
        this.listDataHeader = listDataHeader;
        this.listDataChild = listDataChild;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.from(getContext()).inflate(R.layout.fragment_main,null);
        expandableListView=view.findViewById(R.id.lvExp);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        expandableAdapter=new ExpandableAdapter(getActivity(),listDataHeader,listDataChild);

        expandableListView.setAdapter(expandableAdapter);

    }
}
