package com.example.rothnaksomang.homework8.fragment;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.rothnaksomang.homework8.R;
import com.example.rothnaksomang.homework8.callback.MainCategoryCallback;
import com.example.rothnaksomang.homework8.entity.maincategory.DataItem;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;

public class FragmentInsertMainCategory extends Fragment {

    View view;
    LayoutInflater inflater;
    MainCategoryCallback mainCategoryCallback;
    Uri image;
    String imagePath;
    FragmentInsertMainCategoryViewHolder fragmentInsert;

    public FragmentInsertMainCategory(){}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainCategoryCallback){
            mainCategoryCallback= (MainCategoryCallback) context;
        }else {
            throw  new RuntimeException(context.toString()
            +"Must Implement MainCategoryCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainCategoryCallback=null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.from(getActivity()).inflate(R.layout.fragment_insert_main_category,container,false);
        fragmentInsert=new FragmentInsertMainCategoryViewHolder(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fragmentInsert.btnSubmite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataItem dataItem;
                String name,des;
                File file;
                if(TextUtils.isEmpty(fragmentInsert.etName.getText())){
                    Toast.makeText(getActivity(),"Name are Require!",Toast.LENGTH_SHORT).show();
                    fragmentInsert.etName.requestFocus();
                }else if(TextUtils.isEmpty(fragmentInsert.etDes.getText())){
                    Toast.makeText(getActivity(),"Description are Require!",Toast.LENGTH_SHORT).show();
                    fragmentInsert.etDes.requestFocus();
                }else if(!(imagePath==null)){
                    Toast.makeText(getActivity(),"Image are Require!",Toast.LENGTH_SHORT).show();
                }else{

                    name=fragmentInsert.etName.getText().toString();
                    des=fragmentInsert.etDes.getText().toString();
//                    file=new File(imagePath);
//                    RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//                    MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), requestBody);

                    dataItem=new DataItem();
                    dataItem.setCateName(name);
                    dataItem.setDes(des);

                    mainCategoryCallback.insertMainCategory(dataItem);
//                    mainCategoryCallback.insertMainCategory(fileToUpload,dataItem);
//                    Toast.makeText(getActivity(),"Insert successful 1",Toast.LENGTH_SHORT).show();
                }

            }
        });
        fragmentInsert.btnPickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,0);
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            if(data==null){
                Toast.makeText(getContext(),"Image is require!",Toast.LENGTH_SHORT).show();
                return;
            }
            Uri selectIamge=data.getData();
            fragmentInsert.ivProfile.setImageURI(selectIamge);

            String[] pathColumn={MediaStore.Images.Media.DATA};
            CursorLoader cursorLoader=new CursorLoader(getActivity(),selectIamge,pathColumn,null,null,null);
            Cursor cursor=cursorLoader.loadInBackground();


            if (cursor.moveToFirst()){
                int columnIndex=cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                imagePath=cursor.getString(columnIndex);
            }

        }

    }

    class FragmentInsertMainCategoryViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.etName)
        EditText etName;
        @BindView(R.id.etDes)
        EditText etDes;
        @BindView(R.id.ivProfile)
        ImageView ivProfile;
        @BindView(R.id.btnSubmit)
        Button btnSubmite;
        @BindView(R.id.btnPickImage)
        Button btnPickImage;

        public FragmentInsertMainCategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
