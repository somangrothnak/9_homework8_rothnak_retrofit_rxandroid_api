package com.example.rothnaksomang.homework8.fragment;

import android.content.Context;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.example.rothnaksomang.homework8.R;
import com.example.rothnaksomang.homework8.callback.MainCategoryCallback;
import com.example.rothnaksomang.homework8.entity.maincategory.DataItem;
import com.example.rothnaksomang.homework8.entity.maincategory.update.ResponseUpdateMain;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FragmentUpdate extends Fragment {
//    private DataItem dataItem;
    private List<DataItem> dataItemList;
    MainCategoryCallback mainCategoryCallback;
    LayoutInflater inflater;
    View view;

    public FragmentUpdate(){}

    public void setDataItem(List<DataItem> dataItemList){

        this.dataItemList=dataItemList;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainCategoryCallback){
            mainCategoryCallback= (MainCategoryCallback) context;
        }else {
            throw  new RuntimeException(context.toString()
                    +"Must Implement MainCategoryCallback");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view=inflater.from(getActivity()).inflate(R.layout.layout_main_item_update,container,false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        int id=0;

        FragmentUpdateViewHolder fragmentUpdate=new FragmentUpdateViewHolder(view);
        List<String> titleMainList = new ArrayList<>();
        for (DataItem dataitem:dataItemList) {
            titleMainList.add(dataitem.getCateName());
        }
        ArrayAdapter arrayAdapter=new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,titleMainList);

        if(fragmentUpdate!=null){
            fragmentUpdate.spinner.setAdapter(arrayAdapter);
            loadImageFromURL(dataItemList.get(0).getIconName(),fragmentUpdate.ivMainCategory);
            fragmentUpdate.tvMainTitle.setText(dataItemList.get(0).getCateName());

            fragmentUpdate.btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataItem dataItem=new DataItem();

                    if(TextUtils.isEmpty(fragmentUpdate.tvMainTitle.getText())){
                        Toast.makeText(getActivity(),"Name are Require!",Toast.LENGTH_SHORT).show();
                        fragmentUpdate.tvMainTitle.requestFocus();
                    }else{
                        dataItem.setCateName(fragmentUpdate.tvMainTitle.getText().toString());
                        dataItem.setId(dataItemList.get(id).getId());
                        dataItem.setDes(fragmentUpdate.tvMainDes.getText().toString());
                        dataItem.setStatus(true);
                        dataItem.setIconName(dataItemList.get(id).getIconName());

                        mainCategoryCallback.updateMainCategory(dataItem);
//                        Toast.makeText(getActivity(),"Update Main successful 1",Toast.LENGTH_SHORT).show();

                    }

                }
            });

            fragmentUpdate.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(getActivity(),dataItemList.get(position).getCateName(),Toast.LENGTH_SHORT).show();
                    fragmentUpdate.tvMainTitle.setText(dataItemList.get(position).getCateName());
                    loadImageFromURL(dataItemList.get(position).getIconName(),fragmentUpdate.ivMainCategory);
                    id=position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }

    }

    class FragmentUpdateViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.sp_main_category)
        Spinner spinner;
        @BindView(R.id.iv_main_category)
        ImageView ivMainCategory;
        @BindView(R.id.btn_update)
        Button btnUpdate;
        @BindView(R.id.et_main_title)
        EditText tvMainTitle;
        @BindView(R.id.et_main_des)
        EditText tvMainDes;

        public FragmentUpdateViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
    public void loadImageFromURL(String fileUrl, ImageView iv){
        Picasso.with(getActivity()).load(fileUrl).into(iv);
    }
}
