package com.example.rothnaksomang.homework8.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rothnaksomang.homework8.R;
import com.example.rothnaksomang.homework8.callback.MainCategoryCallback;
import com.example.rothnaksomang.homework8.entity.maincategory.DataItem;
import com.example.rothnaksomang.homework8.entity.maincategory.insert.ResponseInsertSub;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentInsertSubCategory extends Fragment {
    private List<DataItem> dataItemList;
    MainCategoryCallback mainCategoryCallback;
    LayoutInflater inflater;
    View view;

    public FragmentInsertSubCategory(){}

    public void setDataItem(List<DataItem> dataItemList){

        this.dataItemList=dataItemList;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainCategoryCallback){
            mainCategoryCallback= (MainCategoryCallback) context;
        }else {
            throw  new RuntimeException(context.toString()
                    +"Must Implement MainCategoryCallback");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view=inflater.from(getActivity()).inflate(R.layout.fragment_insert_sub_category,container,false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final int[] id = {0};

        FragmentInsertViewHolder fragmentInsert=new FragmentInsertViewHolder(view);
        List<String> titleMainList = new ArrayList<>();
        for (DataItem dataitem:dataItemList) {
            titleMainList.add(dataitem.getCateName());
        }
        ArrayAdapter arrayAdapter=new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,titleMainList);

        if(fragmentInsert!=null){
            fragmentInsert.spinner.setAdapter(arrayAdapter);
            id[0]=0;
            fragmentInsert.btnInsert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(TextUtils.isEmpty(fragmentInsert.etName.getText())){
                        Toast.makeText(getActivity(),"Name is Require!",Toast.LENGTH_SHORT).show();
                    }else if(TextUtils.isEmpty(fragmentInsert.etDes.getText())){
                        Toast.makeText(getActivity(),"Description is Require!",Toast.LENGTH_SHORT).show();
                    }else{
                        ResponseInsertSub responseInsertSub=new ResponseInsertSub();
                        responseInsertSub.setCateName(fragmentInsert.etName.getText().toString());
                        responseInsertSub.setDes(fragmentInsert.etDes.getText().toString());
                        responseInsertSub.setStatus(true);
                        responseInsertSub.setMainCateId(dataItemList.get(id[0]).getId());
                        responseInsertSub.setIconName(null);
                        mainCategoryCallback.insertSubCategory(responseInsertSub);
//                        Toast.makeText(getActivity(),"Insert Sub Success",Toast.LENGTH_SHORT).show();
                    }

                }
            });

            fragmentInsert.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(getActivity(),dataItemList.get(position).getCateName(),Toast.LENGTH_SHORT).show();
                    id=position;

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }

    }

    class FragmentInsertViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.sp_main_category_frag_insert_sub_cate)
        Spinner spinner;
        @BindView(R.id.btnSubmit_sub_category)
        Button btnInsert;
        @BindView(R.id.etName)
        TextView etName;
        @BindView(R.id.etDes)
        TextView etDes;

        public FragmentInsertViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
