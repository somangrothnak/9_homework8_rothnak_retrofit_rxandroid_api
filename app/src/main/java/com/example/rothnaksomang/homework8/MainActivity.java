package com.example.rothnaksomang.homework8;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.rothnaksomang.homework8.callback.MainCategoryCallback;
import com.example.rothnaksomang.homework8.entity.maincategory.DataItem;
import com.example.rothnaksomang.homework8.entity.maincategory.Response;
import com.example.rothnaksomang.homework8.entity.maincategory.SubCateItem;
import com.example.rothnaksomang.homework8.entity.maincategory.insert.ResponseInsertSub;
import com.example.rothnaksomang.homework8.entity.maincategory.update.ResponseUpdateMain;
import com.example.rothnaksomang.homework8.fragment.FragmentInsertMainCategory;
import com.example.rothnaksomang.homework8.fragment.FragmentInsertSubCategory;
import com.example.rothnaksomang.homework8.fragment.FragmentMain;
import com.example.rothnaksomang.homework8.fragment.FragmentUpdate;
import com.example.rothnaksomang.homework8.service.CategoryGenerator;
import com.example.rothnaksomang.homework8.service.CategoryService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;

public class MainActivity extends AppCompatActivity implements MainCategoryCallback {
    CategoryService categoryService;
    private CompositeDisposable disposable = new CompositeDisposable();
    public List<DataItem> mainCategory;
    HashMap<String, List<SubCateItem>> listHashMap;
    FragmentMain fragmentMain;
    FragmentUpdate fragmentUpdate;
    FragmentInsertMainCategory fragmentInsertMainCategory;
    FragmentInsertSubCategory fragmentInsertSubCategory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listHashMap=new HashMap<>();
        mainCategory=new ArrayList<>();

        categoryService = CategoryGenerator.createService(CategoryService.class);
        getCategoryRx();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.option_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.mnUpdate:
                fragmentUpdate=new FragmentUpdate();
                fragmentUpdate.setDataItem(mainCategory);
                replaceFragment(fragmentUpdate);
                return true;
            case R.id.mnAddNewMain:
                fragmentInsertMainCategory=new FragmentInsertMainCategory();
                //use Retrofit to Insert Data
                replaceFragment(fragmentInsertMainCategory);
                return true;
            case R.id.mnAddNewSub:
                fragmentInsertSubCategory=new FragmentInsertSubCategory();
                fragmentInsertSubCategory.setDataItem(mainCategory);
                //use Retrofit to Insert Data
                replaceFragment(fragmentInsertSubCategory);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void getCategoryRx() {
//  use RxAndroid with Retrofit
        disposable.add(
                categoryService.getCategoryRx()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<Response>() {
                            @Override
                            public void onNext(Response response) {
//                          get Value from API
                                mainCategory = response.getData();
                                for (DataItem dataItem:mainCategory) {
                                    List<SubCateItem> subCateItemList =new ArrayList<>();
                                    subCateItemList=dataItem.getSubCate();
                                    String titleMain=dataItem.getCateName();
                                    listHashMap.put(titleMain,subCateItemList);
                                }
//                          call default Fragment
                                fragmentMain=new FragmentMain();
                                fragmentMain.setValue(mainCategory,listHashMap);
                                replaceFragment(fragmentMain);

                                Toast.makeText(getApplicationContext(), "MainCategory Successful", Toast.LENGTH_SHORT).show();
//
                            }

                            @Override
                            public void onComplete() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }
                        })

        );
    }
    public void InsertMainCategory(DataItem dataItem){
        disposable.add(
            categoryService.insertMainCategory(dataItem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<DataItem>(){
                    @Override
                    public void onNext(DataItem dataItem) {
                        Toast.makeText(getApplicationContext()," Insert Success 2",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getApplicationContext()," Insert not Success",Toast.LENGTH_SHORT).show();

                    }
                })
        );
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }

//Method to replace Fragment
    public void replaceFragment(Fragment destFragment){
        FragmentManager fragmentManager=this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_parent,destFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void insertMainCategory(DataItem dataItem) {
        InsertMainCategory(dataItem);
    }

    @Override
    public void insertMainCategory(MultipartBody.Part fileToUpload, DataItem dataItem) {

    }

    @Override
    public void insertSubCategory(ResponseInsertSub responseInsertSub) {
        disposable.add(
                categoryService.insertSubCategory(responseInsertSub)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<ResponseInsertSub>(){
                            @Override
                            public void onNext(ResponseInsertSub responseInsertSub1) {
                                Toast.makeText(getApplicationContext()," Insert Sub Success 2",Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onComplete() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                Toast.makeText(getApplicationContext()," Insert not Success",Toast.LENGTH_SHORT).show();
                            }
                        })
        );
    }

    @Override
    public void updateMainCategory(DataItem dataItem) {
        disposable.add(
                categoryService.updateMain(dataItem)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<DataItem>(){
                            @Override
                            public void onNext(DataItem dataItem1) {
                                Toast.makeText(getApplicationContext()," Update Main Success 2",Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onComplete() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                Toast.makeText(getApplicationContext()," Update Main not Success",Toast.LENGTH_SHORT).show();
                            }
                        })
        );
    }
}
