package com.example.rothnaksomang.homework8.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.rothnaksomang.homework8.R;
import com.example.rothnaksomang.homework8.entity.maincategory.DataItem;
import com.example.rothnaksomang.homework8.entity.maincategory.SubCateItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MainCategoryAdapter extends RecyclerView.Adapter<MainCategoryAdapter.MainCageoryVIewHoler> {
    Context context;
    LayoutInflater inflater;
    List<DataItem> itemList;
    List<SubCateItem> subCateItemList;
    List<String> mainTitleList;



    public MainCategoryAdapter(Context context, List<DataItem> itemList){
        this.context=context;
        this.itemList=itemList;
        inflater=LayoutInflater.from(context);

    }
    public MainCategoryAdapter(List<String> mainTitleList){
        this.mainTitleList=mainTitleList;
    }


    @NonNull
    @Override
    public MainCageoryVIewHoler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=inflater.inflate(R.layout.layout_main_item,null);
        return new MainCageoryVIewHoler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainCageoryVIewHoler mainCageoryVIewHoler, int i) {
        //Load Image with Picasso
        loadImageFromURL(itemList.get(i).getIconName(),mainCageoryVIewHoler.imageButton);

        String mainTitle=itemList.get(i).getCateName().toString();
        mainTitleList=new ArrayList<>();
        mainTitleList.add(mainTitle);

        List<SubCateItem> subCateItem=itemList.get(i).getSubCate();
        for(SubCateItem subCateItem1:subCateItem){
            mainTitleList.add(subCateItem1.getCateName());
        }

        ArrayAdapter arrayAdapter=new ArrayAdapter(context,R.layout.support_simple_spinner_dropdown_item,mainTitleList);
        mainCageoryVIewHoler.spMainCategory.setAdapter(arrayAdapter);

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class MainCageoryVIewHoler extends RecyclerView.ViewHolder{
        AppCompatImageButton imageButton;
        Spinner spMainCategory;
        CardView cvItem;
        LinearLayout llItem;

        public MainCageoryVIewHoler(@NonNull View itemView) {
            super(itemView);
            imageButton=itemView.findViewById(R.id.img);
            spMainCategory=itemView.findViewById(R.id.sp_category_name);
            cvItem=itemView.findViewById(R.id.cv_item);
            llItem=itemView.findViewById(R.id.ll_item);

        }
    }
    public void loadImageFromURL(String fileUrl,
                                    ImageView iv){
        Picasso.with(context).load(fileUrl).into(iv);
    }
}
