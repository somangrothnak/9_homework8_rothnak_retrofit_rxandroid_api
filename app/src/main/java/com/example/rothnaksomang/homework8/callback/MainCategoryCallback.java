package com.example.rothnaksomang.homework8.callback;

import com.example.rothnaksomang.homework8.entity.maincategory.DataItem;
import com.example.rothnaksomang.homework8.entity.maincategory.insert.ResponseInsertSub;

import okhttp3.MultipartBody;

public interface MainCategoryCallback {
    void insertMainCategory(DataItem dataItem);
    void insertMainCategory(MultipartBody.Part fileToUpload,DataItem dataItem);
    void insertSubCategory(ResponseInsertSub responseInsertSub);
    void updateMainCategory(DataItem dataItem);
}
