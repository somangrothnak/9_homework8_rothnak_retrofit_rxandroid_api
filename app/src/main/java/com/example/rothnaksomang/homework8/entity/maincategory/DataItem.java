package com.example.rothnaksomang.homework8.entity.maincategory;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataItem{

	@SerializedName("des")
	private String des;

	@SerializedName("cate_name")
	private String cateName;

	@SerializedName("sub_cate")
	private List<SubCateItem> subCate;

	@SerializedName("id")
	private int id;

	@SerializedName("icon_name")
	private String iconName;

	@SerializedName("status")
	private boolean status;

	public void setDes(String des){
		this.des = des;
	}

	public String getDes(){
		return des;
	}

	public void setCateName(String cateName){
		this.cateName = cateName;
	}

	public String getCateName(){
		return cateName;
	}

	public void setSubCate(List<SubCateItem> subCate){
		this.subCate = subCate;
	}

	public List<SubCateItem> getSubCate(){
		return subCate;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIconName(String iconName){
		this.iconName = iconName;
	}

	public String getIconName(){
		return iconName;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SubDataItem{" +
			"des = '" + des + '\'' + 
			",cate_name = '" + cateName + '\'' + 
			",sub_cate = '" + subCate + '\'' + 
			",id = '" + id + '\'' + 
			",icon_name = '" + iconName + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}