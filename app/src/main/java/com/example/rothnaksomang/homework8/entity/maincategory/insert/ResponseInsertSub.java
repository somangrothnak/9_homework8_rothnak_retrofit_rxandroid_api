package com.example.rothnaksomang.homework8.entity.maincategory.insert;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseInsertSub {

	@SerializedName("des")
	private String des;

	@SerializedName("keywords")
	private List<String> keywords;

	@SerializedName("main_cate_id")
	private int mainCateId;

	@SerializedName("cate_name")
	private String cateName;

	@SerializedName("id")
	private int id;

	@SerializedName("icon_name")
	private String iconName;

	@SerializedName("status")
	private boolean status;

	public void setDes(String des){
		this.des = des;
	}

	public String getDes(){
		return des;
	}

	public void setKeywords(List<String> keywords){
		this.keywords = keywords;
	}

	public List<String> getKeywords(){
		return keywords;
	}

	public void setMainCateId(int mainCateId){
		this.mainCateId = mainCateId;
	}

	public int getMainCateId(){
		return mainCateId;
	}

	public void setCateName(String cateName){
		this.cateName = cateName;
	}

	public String getCateName(){
		return cateName;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIconName(String iconName){
		this.iconName = iconName;
	}

	public String getIconName(){
		return iconName;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResponseInsertSub{" +
			"des = '" + des + '\'' + 
			",keywords = '" + keywords + '\'' + 
			",main_cate_id = '" + mainCateId + '\'' + 
			",cate_name = '" + cateName + '\'' + 
			",id = '" + id + '\'' + 
			",icon_name = '" + iconName + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}