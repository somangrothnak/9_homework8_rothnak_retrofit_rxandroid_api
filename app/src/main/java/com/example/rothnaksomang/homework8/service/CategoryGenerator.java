package com.example.rothnaksomang.homework8.service;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class CategoryGenerator {
    final static String url="http://110.74.194.125:15000";
    private static Retrofit.Builder builder=new Retrofit.Builder()
            .baseUrl(url)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass){
        return  builder.build().create(serviceClass);
    }
}
