package com.example.rothnaksomang.homework8.service;

import com.example.rothnaksomang.homework8.entity.maincategory.DataItem;
import com.example.rothnaksomang.homework8.entity.maincategory.Response;
import com.example.rothnaksomang.homework8.entity.maincategory.SubCateItem;
import com.example.rothnaksomang.homework8.entity.maincategory.insert.ResponseInsertSub;
import com.example.rothnaksomang.homework8.entity.maincategory.update.ResponseUpdateMain;
import com.google.gson.JsonObject;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface CategoryService {

    @GET("api/v1/categories")
    Observable<Response> getCategoryRx();

    @POST("api/v1/categories/create-main")
    Observable<DataItem> insertMainCategory(@Body DataItem dataItem);

    @PUT("api/v1/categories/update-main")
    Observable<DataItem> updateMain(@Body DataItem dataItem);

    @POST("api/v1/categories/create-sub")
    Observable<ResponseInsertSub> insertSubCategory(@Body ResponseInsertSub responseInsertSub);

    @Multipart
    @PUT("api/v1/urls/{id}")
    Observable<JsonObject> uploadIamge(@Part MultipartBody.Part file,@Path("id") Integer id);






//    @PUT("")
//    Observable<DataItem> updateMainCateogry(@Path("id") String id,@Bodi)

}
